<?php
    session_start();

    $id = $_SESSION['user_id'];

    $servename="localhost";
    $usernamedb="root";
    $passworddb="";
    $dbname="office";

    //create connection
    $conn = new mysqli($servename,$usernamedb,$passworddb,$dbname);

    if($conn->connect_error){
        die("connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM `hobby_tbl` WHERE user_id = $id";

    // die($sql);

    $result = mysqli_query($conn, $sql);

    $saved_hobbies = array();

    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            array_push($saved_hobbies, $row);
        }
    }
?>

<br>
<html>
<head>
<title>first work</title>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body style="padding-top: 70px; padding-bottom: 70px;">
	<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
    <span class="sr-only">Toggle navigation</span> 
    <span class="icon-bar"></span> 
    <span class="icon-bar"></span> 
    <span class="icon-bar"></span> </button>
    <a class="navbar-brand" href="user.php" style="color:blue">Hobby List</a> 
    </div> 
</div>
</nav>
<br/> <br/> <br/>

<br/>
<div class="container jumbotron">
    <!-- <form  action="" method="post"> -->
        <div class="form-group has-feedback">
            <br>
            <?php
            foreach($saved_hobbies as $hobby)
            {
                
                echo "<label class='control-label' style='font-weight: bold;' >" . $hobby['name'] . "</label>";
            }   
        ?>
        <br>
        </div>
        <nav class="navbar navbar-default navbar-inverse navbar-fixed-bottom">
          <div class="container">
            <div class="navbar-header"><a class="navbar-brand" style="color:blue">Hobby</a></div>
            <ul class="nav navbar-nav navbar-right"> 
            <li> &copy; 2018. All rights reserved.</a></li>
          </ul> 
          </div>
        </nav>

<!-- </form> -->
</div>
</body>
</html>